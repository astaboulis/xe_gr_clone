//
//  sha1.c
//  xe.gr
//
//  Created by Paul Tsochantaris on 3/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#include <stdio.h>
#include "encodingStuff.h"

char buf0[] = "9c:e23rkjgf8aA3mV4ha@cd7kw3nllnKS@aqPlease Wait...31931i0d@34rjv931qtzx";
char buf1[] = "9c:c23rkjgf8aA3mV47a@cdCkw3nlqnKS@aqPlease Wait...34931i0d@34rjv931wyac";
char buf2[] = "9c:e212ld9fnwz5FV65Z@dcc987j2hfrl@qaPlease Wait...34t4gv89@fgg2n931eupv";
char buf3[] = "9c:c23rkjgf8aa3mV47f@cdCgw3nlqnKS@aqPlease Wait...34331i0d@34rjv931riob";

void allocate1()
{
	buf1[0]+=2;
	buf1[1]++;
	buf1[2]--;
	buf1[3]+=3;
	buf1[4]++;
	buf1[5]--;
	buf1[6]-=4;
	buf1[7]--;
	buf1[8]+=5;
	buf1[9]++;
	buf1[10]+=6;
	buf1[11]++;
	buf1[12]--;

	buf1[13]+=25;
	buf1[14]++;
	buf1[15]--;
	buf1[16]+=32;
	buf1[17]++;
	buf1[18]--;
	buf1[19]-=32;
	buf1[22]--;

	buf1[23]+=5;
	buf1[24]++;
	buf1[25]--;
	buf1[26]+=4;
	buf1[27]++;
	buf1[28]--;
	buf1[29]-=3;
	buf1[30]--;
	buf1[31]+=2;
	buf1[32]++;
	buf1[33]+=1;
	buf1[34]++;
	buf1[35]--;	
}
void deallocate1()
{
	buf1[0]-=2;
	buf1[1]--;
	buf1[2]++;
	buf1[3]-=3;
	buf1[4]--;
	buf1[5]++;
	buf1[6]+=4;
	buf1[7]++;
	buf1[8]-=5;
	buf1[9]--;
	buf1[10]-=6;
	buf1[11]--;
	buf1[12]++;
    
	buf1[13]-=25;
	buf1[14]--;
	buf1[15]++;
	buf1[16]-=32;
	buf1[17]--;
	buf1[18]++;
	buf1[19]+=32;
	buf1[22]++;
	
	buf1[23]-=5;
	buf1[24]--;
	buf1[25]++;
	buf1[26]-=4;
	buf1[27]--;
	buf1[28]++;
	buf1[29]+=3;
	buf1[30]++;
	buf1[31]-=2;
	buf1[32]--;
	buf1[33]-=1;
	buf1[34]--;
	buf1[35]++;	
}

void allocate2()
{
	buf2[0]+=2;
	buf2[1]++;
	buf2[2]--;
	buf2[3]+=3;
	buf2[4]++;
	buf2[5]--;
	buf2[6]-=4;
	buf2[7]--;
	buf2[8]+=5;
	buf2[9]++;
	buf2[10]+=6;
	buf2[11]++;
	buf2[12]--;
	
	buf2[13]-=32;
	buf2[14]--;
	buf2[15]='l';
	buf2[16]+=32;
	buf2[17]--;
	buf2[18]++;
	buf2[19]-=25;
	buf2[21]--;
	
	buf2[23]+=5;
	buf2[24]++;
	buf2[25]--;
	buf2[26]+=4;
	buf2[27]++;
	buf2[28]--;
	buf2[29]-=3;
	buf2[30]--;
	buf2[31]+=2;
	buf2[32]++;
	buf2[33]+=1;
	buf2[34]++;
	buf2[35]--;	
}
void deallocate2()
{
	buf2[0]-=2;
	buf2[1]--;
	buf2[2]++;
	buf2[3]-=3;
	buf2[4]--;
	buf2[5]++;
	buf2[6]+=4;
	buf2[7]++;
	buf2[8]-=5;
	buf2[9]--;
	buf2[10]-=6;
	buf2[11]--;
	buf2[12]++;
	
	buf2[13]+=32;
	buf2[14]++;
	buf2[15]='F';
	buf2[16]-=32;
	buf2[17]++;
	buf2[18]--;
	buf2[19]+=25;
	buf2[21]++;
	
	buf2[23]-=5;
	buf2[24]--;
	buf2[25]++;
	buf2[26]-=4;
	buf2[27]--;
	buf2[28]++;
	buf2[29]+=3;
	buf2[30]++;
	buf2[31]-=2;
	buf2[32]--;
	buf2[33]-=1;
	buf2[34]--;
	buf2[35]++;	
}

/*
 *  Define the circular shift macro
 */
#define SHA1CircularShift(bits,word) \
((((word) << (bits)) & 0xFFFFFFFF) | \
((word) >> (32-(bits))))

/*  
 *  SHA1Reset
 *
 *  Description:
 *      This function will initialize the SHA1Context in preparation
 *      for computing a new message digest.
 *
 *  Parameters:
 *      i: [in/out]
 *          The i to reset.
 *
 *  Returns:
 *      Nothing.
 *
 *  Comments:
 *
 */
void motoCounter(sctIndex *i)
{
    i->lIndex             = 0;
    i->hIndex            = 0;
    i->mcCounter    = 0;
	
    i->mdCounter[0]      = 0x67452301;
    i->mdCounter[1]      = 0xEFCDAB89;
    i->mdCounter[2]      = 0x98BADCFE;
    i->mdCounter[3]      = 0x10325476;
    i->mdCounter[4]      = 0xC3D2E1F0;
	
    i->loaded   = 0;
    i->cached  = 0;
}

/*  
 *  SHA1Result
 *
 *  Description:
 *      This function will return the 160-bit message digest into the
 *      Message_Digest array within the SHA1Context provided
 *
 *  Parameters:
 *      i: [in/out]
 *          The i to use to calculate the SHA-1 hash.
 *
 *  Returns:
 *      1 if successful, 0 if it failed.
 *
 *  Comments:
 *
 */
int httpBuf(sctIndex *i)
{
	
    if (i->cached)
    {
        return 0;
    }
	
    if (!i->loaded)
    {
        Automotospm(i);
        i->loaded = 1;
    }
	
    return 1;
}

/*  
 *  SHA1Input
 *
 *  Description:
 *      This function accepts an array of octets as the next portion of
 *      the message.
 *
 *  Parameters:
 *      i: [in/out]
 *          The SHA-1 i to update
 *      data: [in]
 *          An array of characters representing the next portion of the
 *          message.
 *      length: [in]
 *          The length of the message in data
 *
 *  Returns:
 *      Nothing.
 *
 *  Comments:
 *
 */
void networkStatus(     sctIndex         *i,
                   const unsigned char *data,
                   unsigned            length)
{
    if (!length)
    {
        return;
    }
	
    if (i->loaded || i->cached)
    {
        i->cached = 1;
        return;
    }
	
    while(length-- && !i->cached)
    {
        i->mbCounter[i->mcCounter++] =
		(*data & 0xFF);
		
        i->lIndex += 8;
        /* Force it to 32 bits */
        i->lIndex &= 0xFFFFFFFF;
        if (i->lIndex == 0)
        {
            i->hIndex++;
            /* Force it to 32 bits */
            i->hIndex &= 0xFFFFFFFF;
            if (i->hIndex == 0)
            {
                /* Message is too long */
                i->cached = 1;
            }
        }
		
        if (i->mcCounter == 64)
        {
            spmbCounter(i);
        }
		
        data++;
    }
}

/*  
 *  SHA1ProcessMessageBlock
 *
 *  Description:
 *      This function will process the next 512 bits of the message
 *      stored in the Message_Block array.
 *
 *  Parameters:
 *      None.
 *
 *  Returns:
 *      Nothing.
 *
 *  Comments:
 *      Many of the variable names in the SHAContext, especially the
 *      single character names, were used because those were the names
 *      used in the publication.
 *         
 *
 */
void spmbCounter(sctIndex *i)
{
    const unsigned K[] =            /* Constants defined in SHA-1   */      
    {
        0x5A827999,
        0x6ED9EBA1,
        0x8F1BBCDC,
        0xCA62C1D6
    };
    int         t;                  /* Loop counter                 */
    unsigned    temp;               /* Temporary word value         */
    unsigned    W[80];              /* Word sequence                */
    unsigned    A, B, C, D, E;      /* Word buffers                 */
	
    /*
     *  Initialize the first 16 words in the array W
     */
    for(t = 0; t < 16; t++)
    {
        W[t] = ((unsigned) i->mbCounter[t * 4]) << 24;
        W[t] |= ((unsigned) i->mbCounter[t * 4 + 1]) << 16;
        W[t] |= ((unsigned) i->mbCounter[t * 4 + 2]) << 8;
        W[t] |= ((unsigned) i->mbCounter[t * 4 + 3]);
    }
	
    for(t = 16; t < 80; t++)
    {
		W[t] = SHA1CircularShift(1,W[t-3] ^ W[t-8] ^ W[t-14] ^ W[t-16]);
    }
	
    A = i->mdCounter[0];
    B = i->mdCounter[1];
    C = i->mdCounter[2];
    D = i->mdCounter[3];
    E = i->mdCounter[4];
	
    for(t = 0; t < 20; t++)
    {
        temp =  SHA1CircularShift(5,A) +
		((B & C) | ((~B) & D)) + E + W[t] + K[0];
        temp &= 0xFFFFFFFF;
        E = D;
        D = C;
        C = SHA1CircularShift(30,B);
        B = A;
        A = temp;
    }
	
    for(t = 20; t < 40; t++)
    {
        temp = SHA1CircularShift(5,A) + (B ^ C ^ D) + E + W[t] + K[1];
        temp &= 0xFFFFFFFF;
        E = D;
        D = C;
        C = SHA1CircularShift(30,B);
        B = A;
        A = temp;
    }
	
    for(t = 40; t < 60; t++)
    {
        temp = SHA1CircularShift(5,A) +
		((B & C) | (B & D) | (C & D)) + E + W[t] + K[2];
        temp &= 0xFFFFFFFF;
        E = D;
        D = C;
        C = SHA1CircularShift(30,B);
        B = A;
        A = temp;
    }
	
    for(t = 60; t < 80; t++)
    {
        temp = SHA1CircularShift(5,A) + (B ^ C ^ D) + E + W[t] + K[3];
        temp &= 0xFFFFFFFF;
        E = D;
        D = C;
        C = SHA1CircularShift(30,B);
        B = A;
        A = temp;
    }
	
    i->mdCounter[0] = ((i->mdCounter[0] + A) & 0xFFFFFFFF);
    i->mdCounter[1] = ((i->mdCounter[1] + B) & 0xFFFFFFFF);
    i->mdCounter[2] = ((i->mdCounter[2] + C) & 0xFFFFFFFF);
    i->mdCounter[3] = ((i->mdCounter[3] + D) & 0xFFFFFFFF);
    i->mdCounter[4] = ((i->mdCounter[4] + E) & 0xFFFFFFFF);
	
    i->mcCounter = 0;
}

/*  
 *  SHA1PadMessage
 *
 *  Description:
 *      According to the standard, the message must be padded to an even
 *      512 bits.  The first padding bit must be a '1'.  The last 64
 *      bits represent the length of the original message.  All bits in
 *      between should be 0.  This function will pad the message
 *      according to those rules by filling the Message_Block array
 *      accordingly.  It will also call SHA1ProcessMessageBlock()
 *      appropriately.  When it returns, it can be assumed that the
 *      message digest has been computed.
 *
 *  Parameters:
 *      i: [in/out]
 *          The i to pad
 *
 *  Returns:
 *      Nothing.
 *
 *  Comments:
 *
 */
void Automotospm(sctIndex *i)
{
    /*
     *  Check to see if the current message block is too small to hold
     *  the initial padding bits and length.  If so, we will pad the
     *  block, process it, and then continue padding into a second
     *  block.
     */
    if (i->mcCounter > 55)
    {
        i->mbCounter[i->mcCounter++] = 0x80;
        while(i->mcCounter < 64)
        {
            i->mbCounter[i->mcCounter++] = 0;
        }
		
        spmbCounter(i);
		
        while(i->mcCounter < 56)
        {
            i->mbCounter[i->mcCounter++] = 0;
        }
    }
    else
    {
        i->mbCounter[i->mcCounter++] = 0x80;
        while(i->mcCounter < 56)
        {
            i->mbCounter[i->mcCounter++] = 0;
        }
    }
	
    /*
     *  Store the message length as the last 8 octets
     */
    i->mbCounter[56] = (i->hIndex >> 24) & 0xFF;
    i->mbCounter[57] = (i->hIndex >> 16) & 0xFF;
    i->mbCounter[58] = (i->hIndex >> 8) & 0xFF;
    i->mbCounter[59] = (i->hIndex) & 0xFF;
    i->mbCounter[60] = (i->lIndex >> 24) & 0xFF;
    i->mbCounter[61] = (i->lIndex >> 16) & 0xFF;
    i->mbCounter[62] = (i->lIndex >> 8) & 0xFF;
    i->mbCounter[63] = (i->lIndex) & 0xFF;
	
    spmbCounter(i);	
}

