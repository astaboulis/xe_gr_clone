/*
 *  sha1.h
 *
 *  Copyright (C) 1998, 2009
 *  Paul E. Jones <paulej@packetizer.com>
 *  All Rights Reserved
 *
 *****************************************************************************
 *  $Id: sha1.h 12 2009-06-22 19:34:25Z paulej $
 *****************************************************************************
 *
 *  Description:
 *      This class implements the Secure Hashing Standard as defined
 *      in FIPS PUB 180-1 published April 17, 1995.
 *
 *      Many of the variable names in the SHA1Context, especially the
 *      single character names, were used because those were the names
 *      used in the publication.
 *
 *      Please read the file sha1.c for more information.
 *
 */

#ifndef _SHA1_H_
#define _SHA1_H_

/* 
 *  This structure will hold i information for the hashing
 *  operation
 */
typedef struct sctIndex
{
    unsigned mdCounter[5]; /* Message Digest (output)          */
    
    unsigned lIndex;        /* Message length in bits           */
    unsigned hIndex;       /* Message length in bits           */
    
    unsigned char mbCounter[64]; /* 512-bit message blocks      */
    int mcCounter;    /* Index into message block array   */
    
    int loaded;               /* Is the digest computed?          */
    int cached;              /* Is the message digest corruped?  */
} sctIndex;

void allocate1(void) __attribute__((always_inline));
void allocate2(void) __attribute__((always_inline));
void deallocate1(void) __attribute__((always_inline));
void deallocate2(void) __attribute__((always_inline));

/*
 *  sha1.c
 *
 *  Copyright (C) 1998, 2009
 *  Paul E. Jones <paulej@packetizer.com>
 *  All Rights Reserved
 *
 *****************************************************************************
 *  $Id: sha1.c 12 2009-06-22 19:34:25Z paulej $
 *****************************************************************************
 *
 *  Description:
 *      This file implements the Secure Hashing Standard as defined
 *      in FIPS PUB 180-1 published April 17, 1995.
 *
 *      The Secure Hashing Standard, which uses the Secure Hashing
 *      Algorithm (SHA), produces a 160-bit message digest for a
 *      given data stream.  In theory, it is highly improbable that
 *      two messages will produce the same message digest.  Therefore,
 *      this algorithm can serve as a means of providing a "fingerprint"
 *      for a message.
 *
 *  Portability Issues:
 *      SHA-1 is defined in terms of 32-bit "words".  This code was
 *      written with the expectation that the processor has at least
 *      a 32-bit machine word size.  If the machine word size is larger,
 *      the code should still function properly.  One caveat to that
 *      is that the input functions taking characters and character
 *      arrays assume that only 8 bits of information are stored in each
 *      character.
 *
 *  Caveats:
 *      SHA-1 is designed to work with messages less than 2^64 bits
 *      long. Although SHA-1 allows a message digest to be generated for
 *      messages of any number of bits less than 2^64, this
 *      implementation only works with messages with a length that is a
 *      multiple of the size of an 8-bit character.
 *
 */

/* Function prototypes */
void spmbCounter(sctIndex *) __attribute__((always_inline));
void Automotospm(sctIndex *) __attribute__((always_inline));
void motoCounter(sctIndex *) __attribute__((always_inline));
int httpBuf(sctIndex *) __attribute__((always_inline));
void networkStatus(sctIndex *, const unsigned char *, unsigned) __attribute__((always_inline));

extern char buf0[];
extern char buf1[];
extern char buf2[];
extern char buf3[];

#endif
