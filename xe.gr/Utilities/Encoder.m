//
//  Encoder.m
//  xe-test
//
//  Created by Vassilis Alexandrof on 10/10/2016.
//  Copyright © 2016 Threenitas. All rights reserved.
//

#import "Encoder.h"
#import "encodingStuff.h"

@interface Encoder ()

@end

@implementation Encoder : NSObject 

unsigned long messageCounter;
bool errorDecision;
NSObject *preFlightLock;

-(NSString*)httpHeader:(NSString *)action :(NSString *)session :(unsigned long) messageCounter{
    NSString *k3;
    if(session != NULL)
    {
        allocate1();
        NSString *k1 = [[NSString stringWithCString:buf1+13 encoding:NSASCIIStringEncoding] substringToIndex:10];
        allocate2();
        deallocate1();
        NSString *k2 = [[NSString stringWithCString:buf2+13 encoding:NSASCIIStringEncoding] substringToIndex:10];
        k3 = [NSString stringWithFormat:@"%@|%@|%lu|%@|%@",k1,session,messageCounter,action,k2];
        deallocate2();
    }
    else
    {
        allocate2();
        NSString *k1 = [[NSString stringWithCString:buf2+13 encoding:NSASCIIStringEncoding] substringToIndex:10];
        allocate1();
        deallocate2();
        NSString *k2 = [[NSString stringWithCString:buf1+13 encoding:NSASCIIStringEncoding] substringToIndex:10];
        k3 = [NSString stringWithFormat:@"%@||%lu|%@|%@",k2,messageCounter,action,k1];
        deallocate1();
    }

//    NSLog(@"zzzz %@", k3);
    int len = (int)[k3 length];
    
    char *buf = malloc(len+1);
    [k3 getCString:buf maxLength:(len+1) encoding:NSASCIIStringEncoding];
    time_t t;
    time(&t);
    srand((int)t);
    sctIndex *s = malloc(sizeof(sctIndex));
    motoCounter(s);
    networkStatus(s, (unsigned char *)buf, len);
    httpBuf(s);
    
    NSString *ret = [[NSString alloc] init];
    
    for(int block=0;block<5;block++)
        ret = [ret stringByAppendingFormat:@"%08X",s->mdCounter[block]];
    free(buf);
    free(s);
    
    //NSLog(@"zzzz First Hash: %@",ret);
    char r1 = ( 15.0 * rand() / ( RAND_MAX + 1.0 ) );
    char r2 = ( 15.0 * rand() / ( RAND_MAX + 1.0 ) );
    char r3 = ( 15.0 * rand() / ( RAND_MAX + 1.0 ) );
    ret = [NSString stringWithFormat:@"%02X%02X%02X%@",r1,r2,r3,ret];
    //NSLog(@"zzzz After Padding: %@",ret);
    unsigned long checksum=0;
    for(int f=0;f<[ret length];f++)
        checksum += [ret characterAtIndex:f];
    ret = [NSString stringWithFormat:@"%@%lX",ret,(checksum % 16)];
    
    //NSLog(@"zzzz Pre Final Hash: %@",ret);
    
    buf = malloc([ret length]+1);
    [ret getCString:buf maxLength:([ret length]+1) encoding:NSASCIIStringEncoding];
    for(int f=0;f<[ret length];f++)
        buf[f]=buf[f]+20;
    ret = [NSString stringWithCString:buf encoding:NSASCIIStringEncoding];
    
    free(buf);
    
//    NSLog(@"zzzz Final Hash: %@",ret);
    return ret;
    
}

@end
