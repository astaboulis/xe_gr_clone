//
//  Encoder.h
//  xe-test
//
//  Created by Vassilis Alexandrof on 10/10/2016.
//  Copyright © 2016 Threenitas. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Encoder;

@interface Encoder : NSObject

-(NSString*)httpHeader:(NSString*)action :(NSString *)session :(unsigned long) messageCounter;
@end
